# vt-globals

## Purpose

Provide a module that defines relatively static global variables for use in other modules and Terraform configurations.

## Description

This module doesn't actually create anything. It just defines variables with default values and "output"'s them.

## Usage Instructions

Copy and paste into your Terraform configuration, insert or update the
variables, and run `terraform init`:

```
module "vt-globals" {
  source = "git::ssh://git@code.vt.edu/devcom/terraform-modules/vt-globals.git"
}
```

Then to refer to values, you would use something like:

```
cidr_blocks = ["${module.vt-globals.vt_ip_address_cidrs}"]
```

## Preconditions and Assumptions

None.

## Inputs
None.

## Outputs

**vt_ip_address_cidrs** - a list of VT IPv4 addresses

## Versions

| Version | Major changes |
| ------- | ------------- |
| 1     | Created module |

